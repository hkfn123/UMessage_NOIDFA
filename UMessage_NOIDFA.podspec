Pod::Spec.new do |s|

  s.name         = "UMessage_NOIDFA"
  s.version      = "1.5.6"
  s.summary      = "友盟推送1.5.0"

  s.description  = <<-DESC
  友盟推送 1.4.0 beta无IDFA
                   DESC

  s.homepage     = "http://dev.umeng.com/push/ios/integration#1_3"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "jajeo" => "hkfn123@gmail.com" }
  s.platform     = :ios, "6.0"
  s.source       = { :git => "https://gitlab.com/hkfn123/UMessage_NOIDFA.git", :tag => "1.5.6" }
  s.source_files  = "UMessage_NOIDFA/*.{h,m}" 
  s.vendored_libraries = 'UMessage_NOIDFA/*.a'
  s.libraries     = "z"
  s.framework     = "UserNotification"

end
